﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    interface IMilkable
    {
        string Name { get; set; }
        int Price { get; set; }
        DateTime ManufDate { get; set; }
        string ShowManuf();
    }

    class Products: IMilkable
    {
        DateTime DefDate = new DateTime(1, 1, 1);
        string _name;
        int _price;
        DateTime _manufdate;
        public string Name { get { return _name; } set { _name = value; } }
        public int Price { get { return _price; } set { _price = value; } }
        public DateTime ManufDate { get { return _manufdate; } set { _manufdate = value; } }
        public string ShowManuf()
        {
            return _manufdate.Date.ToString();
        }
        public Products(DateTime Date, string Name = "Default", int Price = 0)
        {
            _name = Name;
            _price = Price;
            _manufdate = Date;
        }
    }

    interface IComparer
    {
        int Compare(object o1, object o2);
    }

    class ProductsComparerName : IComparer<Products>
    {
        public int Compare(Products x, Products y)
        {
            if (x.Name.Length < y.Name.Length)
            {
                return -1;
            }
            else if (x.Name.Length > y.Name.Length)
            {
                return 1;
            }
            else
            {
                if (x.Price < y.Price)
                    return -1;
                return 0;
            }
        }
    }

    class ProductsComparerPrice : IComparer<Products>
    {
        public int Compare(Products x, Products y)
        {
            if (x.Price < y.Price)
                return -1;
            else if (x.Price > y.Price)
                return 1;
            else
            {
                if (x.Price < y.Price)
                    return -1;
                return 0;
            }
        }
    }

    class ProductsComparerDate : IComparer<Products>
    {
        public int Compare(Products x, Products y)
        {
            if (x.ManufDate < y.ManufDate)
                return -1;
            else if (x.ManufDate > y.ManufDate)
                return 1;
            else
            {
                if (x.ManufDate < y.ManufDate)
                    return -1;
                return 0;
            }
        }
    }

    class Program
    {
        static void Main()
        {
            Random r = new Random();
            Products[] Shit = new Products[] { new Products(DateTime.Now.Date, "Bullshit", 1488),
                new Products(DateTime.Now.Date, "Brother's yoghurt", 5),
                new Products(DateTime.Now.Date, "Kitten's crap", 0),
                new Products(DateTime.Now.Date, "Milk", 60),
                new Products(DateTime.Now.Date, "Kurt Cobain's corpse", int.MaxValue),
                new Products(DateTime.Now.Date, "Pegasus' dildo", 228),
                new Products(DateTime.Now.Date) };

            if (r.Next(0, 2) == 0)
            {
                Array.Sort(Shit, new ProductsComparerName());
                Console.WriteLine("Comparing by name~\n");
            }

            else if (r.Next(0,2) == 1)
            {
                Console.WriteLine("Comparing by price~\n");
                Array.Sort(Shit, new ProductsComparerPrice());
            }

            else
            {
                Console.WriteLine("Comparing by date~\n");
                Array.Sort(Shit, new ProductsComparerDate());
            }

            foreach (Products p in Shit)
                Console.WriteLine("{0} {1} {2}", p.Name, p.Price, p.ManufDate);

            Console.ReadKey();
        }
    }
}

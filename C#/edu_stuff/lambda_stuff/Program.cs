﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        Action<double, double> MyOp;

        static void Main(string[] args)
        {
            Console.WriteLine(Circle.GetArea(double.Parse(Console.ReadLine())));
            Person HUMANOID = new Person();
            HUMANOID.Name = Console.ReadLine();
            HUMANOID.Show();
            Console.WriteLine();
            Multiply(double.Parse(Console.ReadLine()), double.Parse(Console.ReadLine()));
            Console.ReadKey();

            //Samples below

            //Predicate
            Predicate <double> Positive = delegate (double x) { return x > 0; };
            Console.WriteLine(Positive(double.Parse(Console.ReadLine())));

            //Func
            Func<int, int> retFunc = GetFactorial;

            //

            Console.ReadKey();
        }

        static void Operation(double x, double y, Action<double, double> op) => op(x, y);
        static void Add(double x, double y) => Console.WriteLine("Сумма: {0}", x + y);
        static void Substract(double x, double y) => Console.WriteLine("Разность: {0}", x - y);
        static void Multiply(double x, double y) => Console.WriteLine("Произведение: {0}", x * y);
        static void Divide(double x, double y) => Console.WriteLine("Частное: {0}", x / y);
        static void GetAtan(double x, double y) => Console.WriteLine("Арктангенс: {0}", Math.Atan2(y, x)); //Mind the order
        static void GetPow(double x, double y) => Console.WriteLine("Степень {1} числа {0} равна: {2}", y, x, Math.Pow(x, y));
        static int GetFactorial(int x)
        {
            int Result = 1;
            for (int i = 0; i <= x; i++)
                Result *= i;
            return Result;
        }
        static int GetInt(int x, Func<int, int> retFunc)
        {
            int Result = 1;
            if (x > 0)
                Result = retFunc(x);
            return Result;
        }
    }

    class Person
    {
        public string Name { get; set; }
        public void Show() => Console.WriteLine(Name);
    }

    class Circle
    {
        public static double GetArea(double R) => (Math.Pow(R, 2) * Math.PI);
    }
}

/*  * TODO: Implement time & stat showing using Thread()
    * TODO: Optimize stat tracking
    * TODO: Add music playback (from a SoundCloud playlist):
        * TODO: Find a good cross-platform SC API
    * TODO: Clean REFERENCES up
    * TODO: Move out stuff to a special ConsoleUtils class with static methods
    * TODO: Code refactoring
*/

/* Free-to-use track "Visionary" by Rameses B: https://soundcloud.com/ramesesb */

using System;
using System.Threading;
using System.Drawing;
using System.Linq;

namespace course_debute
{
    class Pony //TODO: Make it djent
    {
        Thread _timeThread; //Used to track time in background (in order to decrease stats)

        int _hunger, _iq, _entertaiment;
        string _stat, _name;

        public int Hunger { get { return _hunger; } set { _hunger = value; } }
        public int IQ { get { return _iq; } set { _iq = value; } }
        public int Entertaiment { get { return _entertaiment; } set { _entertaiment = value; } }
        public string Stat { get { return _stat; } set { _stat = value; } }
        public string Name { get { return _name; } set { _name = value; } }

        public void Eat() => _addChars(true);
        public void PlayGames() => _addChars(false);
        public void DoNothing() => _addChars(null);
        public string ReturnStats() => (Name + "\'s stats are: " + Stat + "\nFood: " + Hunger + "\nIQ: " + IQ + "\nEntertaiment: " + Entertaiment);

        void _addChars(bool? State) // State: true == eat, false == play games, null == do H/W
        {
            Stat = "";
            Random _r = new Random();
            switch (State)
            {
                case true:
                    Hunger += 5;
                    Entertaiment++;
                    break;
                case false:
                    Hunger -= 2;
                    Entertaiment += 2;
                    if (_r.Next(0, 1) == 1)
                        IQ--;
                    break;
                case null:
                    Hunger--;
                    Entertaiment--;
                    IQ += 2;
                    break;
            }
            _updateStats();
        }

        void _updateStats()
        {
            if (IQ < 10)
                Stat += "Dumbass; ";
            else if (IQ >= 150)
            {
                Console.WriteLine("You won!");
                Console.ReadKey();
                Environment.Exit(0);
            }
            if (Hunger > 0 & Hunger < 150 & Entertaiment > 0)
                Stat += "Normal condition; ";
            else
            {
                Stat += "BLUEBOT; ";
                Console.WriteLine("You lost X)");
                Console.ReadKey();
                Environment.Exit(228);
            }
        }

        void _timedStatDecrease() //TODO: Null _seconds on an event (calling char's Eat() method)
        {
            int _seconds = 0;
            while (true)
            {
                Thread.Sleep(1000); //Wait for 1 second
                _seconds++;
                if (_seconds >= 45)
                {
                    Hunger--; //Decrease Hunger stat after 45 seconds
                    _seconds = 0;
                }
            }
        }

        public Pony(int SetHunger = 100, int SetIQ = 100, int SetEntertaiment = 100, string SetName = "Background Pony")
        {
            _timeThread = new Thread(_timedStatDecrease);
            Hunger = SetHunger;
            IQ = SetIQ;
            Entertaiment = SetEntertaiment;
            Name = SetName;
            _updateStats();
            _timeThread.Start();
        }
    }

    class MusicPlayer //TODO: Try to implement various player states, etc.
    {
        System.Media.SoundPlayer _musicPlayer;
        bool _playerIsInitialised = false;

        public void StartPlayer(bool IsLooped = false) => _startPlayer(IsLooped);
        public void StopPlayer() => _stopPlayer();
        public void Dispose() => _musicPlayer.Dispose();

        void _initPlayer(System.IO.UnmanagedMemoryStream _soundTrack, bool _startLoopedPlayback = false)
        {
            _musicPlayer = new System.Media.SoundPlayer(_soundTrack);
            _playerIsInitialised = true;
            if (_startLoopedPlayback) //TODO: add proper "looped and not looped" parameters support
                _startPlayer(true);
        }

        void _startPlayer(bool _isLooped = false)
        {
            if (_playerIsInitialised)
            {
                if (_isLooped)
                    _musicPlayer.PlayLooping();
                else
                    _musicPlayer.Play();
            }
        }

        void _stopPlayer() => _musicPlayer.Stop();

        public MusicPlayer(System.IO.UnmanagedMemoryStream SoundTrack, bool StartLoopedPlayback = false)
        {
            _initPlayer(SoundTrack, StartLoopedPlayback);
        }
    }

    class ConsoleUtils
    {
        public static void WriteMessage(string Text, ConsoleColor TextColor, bool ClearConsole = false) => _writeMessage(Text, TextColor, ClearConsole);
        public static int TextMenu(string[] MenuStrings, int StartSelection = 0) => _textMenu(MenuStrings, StartSelection);
        public static void DrawImage(Bitmap SourceImage) => _consoleWriteImage(SourceImage);

        static void _writeMessage(string _text, ConsoleColor _textColor, bool _clearConsole = false)
        {
            Console.ForegroundColor = _textColor;
            Console.WriteLine(_text);
            Console.ResetColor();
            if (_clearConsole)
                Console.Clear();
        }

        static int _textMenu(string[] _menuStrings, int _startSelection = 0)
        {
            Console.WriteLine(); //TODO: Use offsets instead

            bool _complete = false;
            int _topOffset = Console.CursorTop;
            int _bottomOffset = 0;
            ConsoleKeyInfo _key;

            Console.CursorVisible = false;

            if ((_menuStrings.Length) > Console.WindowHeight)
                throw new Exception("Too many items to display!");

            while (!_complete)
            {
                for (int i = 0; i < _menuStrings.Length; i++)
                {
                    if (i == _startSelection)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine(_menuStrings[i]);
                        Console.ResetColor();
                    }
                    else
                        Console.WriteLine(_menuStrings[i]);
                }

                _bottomOffset = Console.CursorTop;
                _key = Console.ReadKey(true); //Read the key

                switch (_key.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (_startSelection > 0)
                            _startSelection--;
                        else
                            _startSelection = (_menuStrings.Length - 1);
                        break;
                    case ConsoleKey.DownArrow:
                        if (_startSelection < (_menuStrings.Length - 1))
                            _startSelection++;
                        else
                            _startSelection = 0;
                        break;
                    case ConsoleKey.Enter:
                        _complete = true;
                        break;
                }
                Console.SetCursorPosition(0, _topOffset);
            }
            Console.SetCursorPosition(0, _bottomOffset);
            Console.CursorVisible = true;
            return _startSelection;
        }

        static void _consoleWritePixel(Color _colorValue) //Most of the code (no, really, like 99,9%) by Antonín Lejsek @ StackOverflow (https://stackoverflow.com/a/33715138)
        {
            int[] _cColors = {
                0x000000,
                0x000080,
                0x008000,
                0x008080,
                0x800000,
                0x800080,
                0x808000,
                0xC0C0C0,
                0x808080,
                0x0000FF,
                0x00FF00,
                0x00FFFF,
                0xFF0000,
                0xFF00FF,
                0xFFFF00,
                0xFFFFFF
            };
            Color[] _cTable = _cColors.Select(_x => Color.FromArgb(_x)).ToArray();
            char[] _rList = new char[] { (char)9617, (char)9618, (char)9619, (char)9608 }; //1/4, 2/4, 3/4, 4/4
            int[] _bestHit = new int[] { 0, 0, 4, int.MaxValue }; //ForeColor, BackColor, Symbol, Score

            for (int _rChar = _rList.Length; _rChar > 0; _rChar--)
            {
                for (int _cFore = 0; _cFore < _cTable.Length; _cFore++)
                {
                    for (int _cBack = 0; _cBack < _cTable.Length; _cBack++)
                    {
                        int R = (_cTable[_cFore].R * _rChar + _cTable[_cBack].R * (_rList.Length - _rChar)) / _rList.Length;
                        int G = (_cTable[_cFore].G * _rChar + _cTable[_cBack].G * (_rList.Length - _rChar)) / _rList.Length;
                        int B = (_cTable[_cFore].B * _rChar + _cTable[_cBack].B * (_rList.Length - _rChar)) / _rList.Length;
                        int _iScore = (_colorValue.R - R) * (_colorValue.R - R) + (_colorValue.G - G) * (_colorValue.G - G) + (_colorValue.B - B) * (_colorValue.B - B);
                        if (!(_rChar > 1 && _rChar < 4 && _iScore > 50000)) //Rule out too weird combinations
                        {
                            if (_iScore < _bestHit[3])
                            {
                                _bestHit[3] = _iScore; //Score
                                _bestHit[0] = _cFore; //ForeColor
                                _bestHit[1] = _cBack; //BackColor
                                _bestHit[2] = _rChar; //Symbol
                            }
                        }
                    }
                }
            }
            Console.ForegroundColor = (ConsoleColor)_bestHit[0];
            Console.BackgroundColor = (ConsoleColor)_bestHit[1];
            Console.Write(_rList[_bestHit[2] - 1]);
        }

        static void _consoleWriteImage(Bitmap _sourceImage)
        {
            int _sMax = 39;
            decimal _percent = Math.Min(decimal.Divide(_sMax, _sourceImage.Width), decimal.Divide(_sMax, _sourceImage.Height));
            Size _dSize = new Size((int)(_sourceImage.Width * _percent), (int)(_sourceImage.Height * _percent));
            Bitmap _bmpMax = new Bitmap(_sourceImage, _dSize.Width * 2, _dSize.Height);
            for (int i = 0; i < _dSize.Height; i++)
            {
                for (int j = 0; j < _dSize.Width; j++)
                {
                    _consoleWritePixel(_bmpMax.GetPixel(j * 2, i));
                    _consoleWritePixel(_bmpMax.GetPixel(j * 2 + 1, i));
                }
                Console.WriteLine();
            }
            Console.ResetColor();
        }
    }

    class MainClass //TODO: make things more abstract
    {
        static MusicPlayer _ponyPlayer;

        static void Main(string[] args) //I doubt we need any args, but still
        {
            int _startSelection = 0;
            _ponyPlayer = new MusicPlayer(Properties.Resources.Visionary, true);
            ConsoleUtils.DrawImage(Properties.Resources.MLPLogo);
            Thread.Sleep(100);
            Console.Clear();
            Console.Write("Enter your character name: ");
            Pony MLP = new Pony(100, 100, 100, Console.ReadLine());
            Console.Clear();
            ConsoleUtils.WriteMessage("Welcome, player!", ConsoleColor.Yellow);
            while (true)
                switch (ConsoleUtils.TextMenu(new string[] { "Show stats", "Eat", "Play games", "Do nothing", "Start player", "Pause player", "Exit" }, _startSelection))
                {
                    case 0: //Show stats
                        Console.Clear();
                        ConsoleUtils.WriteMessage(MLP.ReturnStats(), ConsoleColor.Green);
                        _startSelection = 0;
                        break;
                    case 1: //Eat
                        Console.Clear();
                        MLP.Eat();
                        ConsoleUtils.WriteMessage("Done!", ConsoleColor.Magenta);
                        _startSelection = 1;
                        break;
                    case 2: //Play games
                        Console.Clear();
                        MLP.PlayGames();
                        ConsoleUtils.WriteMessage("Done!", ConsoleColor.Magenta);
                        _startSelection = 2;
                        break;
                    case 3: //Do nothing
                        Console.Clear();
                        MLP.DoNothing();
                        ConsoleUtils.WriteMessage("Done!", ConsoleColor.Magenta);
                        _startSelection = 3;
                        break;
                    case 4: //Start music player
                        Console.Clear();
                        switch (ConsoleUtils.TextMenu(new string[] { "Looped playback", "Normal playback" }))
                        {
                            case 0:
                                _ponyPlayer.StartPlayer(true);
                                break;
                            case 1:
                                _ponyPlayer.StartPlayer();
                                break;
                        }
                        Console.Clear();
                        ConsoleUtils.WriteMessage("Music started", ConsoleColor.Cyan);
                        _startSelection = 4;
                        break;
                    case 5: //Pause music player
                        Console.Clear();
                        _ponyPlayer.StopPlayer();
                        ConsoleUtils.WriteMessage("Music stopped", ConsoleColor.Red);
                        _startSelection = 5;
                        break;
                    case 6: //Exit
                        Console.Clear();
                        _ponyPlayer.Dispose();
                        Environment.Exit(0);
                        break;
                }
        }
    }
}

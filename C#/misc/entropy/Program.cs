/*  * TODO: Use System.Drawing for coloured output => support it
	* TODO: Make it consume as much resources as possible by
			adding some calculations and stuff
	* TODO: Re-create (at least a part of) the song
	* TODO: Code refactoring
	* TODO: Add a new Thread that performs a delayed start of
			another Thread (which is a parameter); DONE:
			* TODO:	Check whether it works properly
	* FIXME: On Linux, some strange output is thrown at the very beginning:
		* TODO: Check on Windows
*/

/*	To compile with CSC, use "csc /reference:System.Numerics.dll Program.cs" */

/*	WARINNG: MAY CONTAIN COMPLETELY UNREADABLE BULLSHIT	*/

using System;
using System.Threading;
using System.IO;
using System.Diagnostics;

namespace entropy
{
	class SLTS_Beep //FIXME, TODO
	{
		void _playNoteF (int _duration) => Console.Beep (175, _duration);
		void _playNoteBb (int _duration) => Console.Beep (233, _duration);
		void _playNoteAb (int _duration) => Console.Beep (207, _duration);
		void _playNoteDb (int _duration) => Console.Beep (277, _duration);

		public void PlayFirstPart (object IsLooped = null)
		{
			if (IsLooped as bool? == true)
				while (true)
					_playFirstPart ();
			else
				_playFirstPart ();
		}

		void _playFirstPart ()
		{
			_playNoteF (500);
			_playNoteBb (1500);
			_playNoteF (500);
			_playNoteF (750);
		}
	}

	class MainClass
	{
		static ConsoleColor [] _ccs = {
			ConsoleColor.Black,
			ConsoleColor.Blue,
			ConsoleColor.Cyan,
			ConsoleColor.DarkBlue,
			ConsoleColor.DarkCyan,
			ConsoleColor.DarkGray,
			ConsoleColor.DarkGreen,
			ConsoleColor.DarkMagenta,
			ConsoleColor.DarkRed,
			ConsoleColor.DarkYellow,
			ConsoleColor.Gray,
			ConsoleColor.Green,
			ConsoleColor.Magenta,
			ConsoleColor.Red,
			ConsoleColor.White,
			ConsoleColor.Yellow
		};

		static Random _r = new Random ();
		static SLTS_Beep _song = new SLTS_Beep ();
		static string _hashedString = "";

		struct BloatVar
		{
			string _randomString;
			string _genRandomString () //FIXME, TODO: Implement OOM error handling
			{
				string _temp = "";
				while (true)
				{
					try
					{
						_temp = new string (_genRandomSymbol (), int.MaxValue);
						return _temp;
					}
					catch (OutOfMemoryException e)
					{
						Console.Write ("Caught OOM exception: {0}", e.Message);
					}
				}
			}
			public string Variable { get { return _randomString = _genRandomString (); } set { _randomString = _genRandomString (); } }
		};

		static char _genRandomSymbol () => (char)_r.Next (0, 255);

		static void _delayedThreadStart (Action _thread, int _delay)
		{
			Thread.Sleep ((int)_delay);
			Thread _tempThread = new Thread (new ThreadStart (_thread));
			_tempThread.Start (); //TODO: check whether it differs from calling _thread()
		}

		static void _changeConsoleColors ()
		{
			while (true)
			{
				Console.BackgroundColor = _ccs [_r.Next (0, _ccs.Length - 1)];
				Console.ForegroundColor = _ccs [_r.Next (0, _ccs.Length - 1)];
			}
		}

		static void _printHashPart ()
		{
			for (int i = 0; i < _hashedString.GetHashCode ().ToString ().Length; i++)
				Console.Write (_hashedString.GetHashCode ().ToString () [i]);
		}

		static void _writeRunAmount ()
		{
			string _filePath = Environment.GetFolderPath (Environment.SpecialFolder.Desktop) + "\\RAMIN MAHASHOL";
			int _amount = 0;
			if (File.Exists (_filePath))
				_amount += int.Parse (File.ReadAllText (_filePath));
			File.WriteAllText (_filePath, (_amount + 1).ToString ());
		}

		static void _duplicate () //TODO: Compare to Fib digits being calculated in separate threads
		{
			//while (true)
				//if (Math.Cos (_r.Next (int.MinValue, int.MaxValue)) == Math.Cos (_r.Next (int.MaxValue, int.MaxValue)))
			Process.Start (AppDomain.CurrentDomain.FriendlyName);
		}

		static void _countFibonacci (object _printResult)
		{
			System.Numerics.BigInteger _fOne = 0, _fTwo = 1, _temp; //Requires System.Numerics reference/dll
			while (true)
			{
				_temp = _fOne;
				_fOne = _fTwo;
				_fTwo += _temp;
				if (_printResult as bool? == true)
					Console.Write (_fOne);
				Thread.Sleep (10);
			}
		}

		static void _controlProcess () //FIXME, but it should work now
		{
			while (true)
				if (Process.GetProcessesByName (AppDomain.CurrentDomain.FriendlyName).Length > 100)
					foreach (Process _process in Process.GetProcessesByName (AppDomain.CurrentDomain.FriendlyName))
						_process.Kill ();
		}

		static void _manipulateHugeString () //TODO: Test it
		{
			BloatVar [] _garbage = new BloatVar [_r.Next (0, int.MaxValue)];
			while (true)
				for (int i = 0; i < _garbage.Length; i++)
					_garbage [i].Variable = _genRandomSymbol ().ToString ();
		}

		public static void Main (string [] args)
		{
			_writeRunAmount ();
			var _music = new Thread (new ParameterizedThreadStart (_song.PlayFirstPart));
			var _changeColor = new Thread (new ThreadStart (_changeConsoleColors));
			var _hash = new Thread (new ThreadStart (_printHashPart));
			var _countFb = new Thread (new ParameterizedThreadStart (_countFibonacci));
			var _manipulate = new Thread (new ThreadStart (_manipulateHugeString));
			var _controlProcessThread = new Thread (new ThreadStart (_controlProcess));
			var _duplicateProcess = new Thread (new ThreadStart (_duplicate));

			Action _parameter = new Action (() => _hash.Start ());
			var _delayedThread = new Thread (() => _delayedThreadStart (_parameter, 1000)); //FIXME
			//_delayedThreadStart (_parameter, 1000);

			_music.Start (true);
			_changeColor.Start ();
			_delayedThread.Start ();
			_countFb.Start (false);
			_manipulate.Start ();
			_controlProcessThread.Start ();
			_duplicateProcess.Start ();

			while (true)
			{
				Console.Write (_genRandomSymbol ());
				if (_hashedString.GetHashCode ().ToString ().Length < Console.BufferHeight - 1)
					_hashedString += _genRandomSymbol ();
				else
					_hashedString = "" + _genRandomSymbol ();
			}
		}
	}
}
